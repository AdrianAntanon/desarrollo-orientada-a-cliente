function isVowelOrNot(word) {
    let wordSplit = word.toLowerCase();
    wordSplit = wordSplit.split('');
    let vowels = '';
    let consonants = '';

    for (let i = 0; i < wordSplit.length; i++) {
        if (wordSplit[i] === 'a' || wordSplit[i] === 'e' || wordSplit[i] === 'i' || wordSplit[i] === 'o' || wordSplit[i] === 'u') {
            vowels += wordSplit[i];
        }else{
            consonants += wordSplit[i];
        }
    }
    let phraseSeparated = `${vowels} ${consonants}`
    return phraseSeparated
}

function splitVowelsFromConsonants(phrase){
    let splitPhrase = isVowelOrNot(phrase);

    console.log(`Frase original: ${phrase}`);

    let vowels = splitPhrase.split(' ')[0];
    let consonants = splitPhrase.slice(vowels.length);

    console.log(`Frase dividida: vocales [${vowels}] y las consonantes(junto con otros carácteres) [${consonants}]`);
}


let word = 'Hola mundo';

splitVowelsFromConsonants(word);
