
function testLength(pass) {
    let wordLength = pass.length;
    if (wordLength <= 20 && wordLength >= 8) {
        return true;
    } else {
        return false;
    }
}

function testMinus(pass) {
    let unicodeLowerA = 97, unicodeLowerZ = 122, count = 0, minimunAllowed = 3;

    for (let index = 0; index < pass.length; index++) {
        let passIsLowerCase = pass.charCodeAt(index);

        if (passIsLowerCase <= unicodeLowerZ && passIsLowerCase >= unicodeLowerA) {
            count++;
        }

    }

    if (count >= minimunAllowed) {
        return true;
    } else {
        return false;
    }

}

function testMayus(pass) {
    let unicodeUpperA = 65, unicodeUpperZ = 90, count = 0, minimunAllowed = 1;
    for (let index = 0; index < pass.length; index++) {
        let passIsUpperCase = pass.charCodeAt(index);

        if (passIsUpperCase <= unicodeUpperZ && passIsUpperCase >= unicodeUpperA) {
            count++;
        }

    }

    if (count >= minimunAllowed) {
        return true;
    } else {
        return false;
    }
}

function testDigit(pass) {
    let unicodeDigit0 = 48, unicodeDigit9 = 57, count = 0, minimunAllowed = 2;
    for (let index = 0; index < pass.length; index++) {
        let passIsDigit = pass.charCodeAt(index);

        if (passIsDigit <= unicodeDigit9 && passIsDigit >= unicodeDigit0) {
            count++;
        }

    }

    if (count >= minimunAllowed) {
        return true;
    } else {
        return false;
    }
}

function testRepetitChars(pass) {
    let counter = 0, repetitChar = 2;
    let char = pass[0];
    for (let index = 0; index < pass.length; index++) {
        char = pass[index];
        if (index >= 1) {
            let previousChar = pass[index - 1];
            if (char === previousChar) {

                counter++;

                if (counter === repetitChar) {
                    return false
                }

            } else {
                counter = 0;
            }

        }

    }
    return true;
}

function checkTrueOrFalse(pass){
    if(pass){
        return "PERMITIDA";
    }else{
        return "DENEGADA";
    }
}

function testPassword(pass){
    console.log("Comprobando contraseña\n")

    let answer;

    let isLength = testLength(pass);
    answer = checkTrueOrFalse(isLength);
    console.log(`Longitud... ${answer}` );

    let isLowerCase = testMinus(pass);
    answer = checkTrueOrFalse(isLowerCase);
    console.log(`Minúsculas... ${answer}` );

    let isUpperCase = testMayus(pass);
    answer = checkTrueOrFalse(isUpperCase);
    console.log(`Mayúsculas... ${answer}` );

    let isRepetitChars = testRepetitChars(pass);
    answer = checkTrueOrFalse(isRepetitChars);
    console.log(`Carácteres repetidos... ${answer}`)

    let isDigits = testDigit(pass);
    answer = checkTrueOrFalse(isDigits);
    console.log(`Dígitos... ${answer}`)


    console.log(`\n\nProcediendo a evaluación final..\n\n`)


    if(isDigits && isLength && isUpperCase && isUpperCase && isRepetitChars){
        console.log(`CONTRASEÑA PERMITIDA`);
    }else{
        console.log(`CONTRASEÑA DENEGADA`);
    }
}

let password = 'abaae11AA';
testPassword(password);