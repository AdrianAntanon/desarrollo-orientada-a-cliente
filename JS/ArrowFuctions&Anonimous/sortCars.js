const listOfCars = [
    ["seat", "Codoba", 1997, 10000],
    ["Kia", "Sorento", 1994, 1000],
    ["seat", "Todelo", 2000, 10000],
    ["Fiat", "Bravo", 2010, 10200],
    ["Fiat", "500", 2010, 10000],
    ["Mercedes", "Calse B", 2001, 39000],
    ["seat", "Ibiza", 1993, 10100],
    ["Alfa Romeo", "Julieta", 2002, 10000],
    ["Alfa Romeo", "159", 2002, 10400],
    ["Mercedes", "Calse C", 2001, 1000],
    ["Alfa Romeo", "147", 1990, 10500],
    ["Fiat", "Punto", 1990, 999],
    ["Citroen", "Saxo", 1980, 10300],
    ["Renault", "Superc 5", 1980, 12000],
    ["BWM", "M3", 2020, 1000],
    ["Kia", "Picanto", 1990, 1000],
    ["Alfa Romeo", "spider", 1970, 14500],
    ["Mercedes", "Calse A", 1994, 60100],
    ["Mercedes", "Calse D", 2011, 21221]
];

const listOfCarsFiltered = listOfCars.filter((car) => {
    if (car[0] !== 'Kia' && car[0] !== 'Alfa Romeo' && car[2] < 2000) {
        return car;
    }
})

const listOfSortedCars = listOfCarsFiltered.sort((carA, carB) => carA[3] - carB[3]);

const listOfDiscountedAppliedCars = listOfSortedCars.map(function (car) {
    car[3] *= 0.8;
    return car;
})

console.log(listOfCarsFiltered);
console.log('\n\n\n');
console.log(listOfSortedCars);
console.log('\n\n\n');
console.log(listOfDiscountedAppliedCars);

const cars = [
    { brand: "seat", model: "Codoba", yearOfProduction: 1997, price: 10000 },
    { brand: "Kia", model: "Sorento", yearOfProduction: 1994, price: 1000 },
    { brand: "seat", model: "Todelo", yearOfProduction: 2000, price: 10000 },
    { brand: "Fiat", model: "Bravo", yearOfProduction: 2010, price: 10200 },
    { brand: "Fiat", model: "500", yearOfProduction: 2010, price: 10000 },
    { brand: "Mercedes", model: "Clase B", yearOfProduction: 2001, price: 39000 },
    { brand: "seat", model: "Ibiza", yearOfProduction: 1993, price: 10100 },
    { brand: "Alfa Romeo", model: "Julieta", yearOfProduction: 2002, price: 10000 },
    { brand: "Alfa Romeo", model: "159", yearOfProduction: 2002, price: 10400 },
    { brand: "Mercedes", model: "Clase C", yearOfProduction: 2001, price: 1000 },
    { brand: "Alfa Romeo", model: "147", yearOfProduction: 1990, price: 10500 },
    { brand: "Fiat", model: "Punto", yearOfProduction: 1990, price: 999 },
    { brand: "Citroen", model: "Saxo", yearOfProduction: 1980, price: 10300 },
    { brand: "Renault", model: "Superc 5", yearOfProduction: 1980, price: 12000 },
    { brand: "BWM", model: "M3", yearOfProduction: 2020, price: 1000 },
    { brand: "Kia", model: "Picanto", yearOfProduction: 1990, price: 1000 },
    { brand: "Alfa Romeo", model: "spider", yearOfProduction: 1970, price: 14500 },
    { brand: "Mercedes", model: "Clase A", yearOfProduction: 1994, price: 60100 },
    { brand: "Mercedes", model: "Clase D", yearOfProduction: 2011, price: 21221 }
];

const filteredCars = cars.filter((element) => {
    if (element.brand !== 'Kia' && element.brand !== 'Alfa Romeo' && element.yearOfProduction < 2000) {
        return element;
    }
});

const sortedCars = filteredCars.sort(element => element.price);


const discountAppliedCars = sortedCars.map(function (element) {
    element.price *= 0.8;
    return element;
})

/*
console.log('Lista original de vehículos');
console.log(cars);
console.log('\n');
console.log('Coches filtrados, sin Kia ni Alfa Romeo y con más de 20 años\n');
console.log(filteredCars);
console.log('\nCoches ordenados por precio');
console.log(sortedCars);
console.log('\nCoches con el descuento aplicado');
console.log(discountAppliedCars);  */
