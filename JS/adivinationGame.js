
function checkInt(num) {
    while (num < 1 || num > 10) {
        console.log('Número no permitido, vuelve a introducirlo, por favor');
    }
}

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low)
}

console.log('Bienvenido al NÚMERO SECRETO, deberás adivinar un número del 1 al 10');

console.log('Introduce un número, por favor')

let randomNum = randomInt(1, 10);

let stdin = process.openStdin();


stdin.addListener("data", function (d) {

    if(d == randomNum){
        console.log('Acertaste')
    }else{
        console.log('Fallaste')
    }
    /* Aquí falta definir el aviso de ACERTADO o FALLADO, en el segundo caso avisarle si está más arriba o más abajo.
    Debería hacer un doble bucle, que el primero sea un while true, el segundo un while num !== randomNum */


});

